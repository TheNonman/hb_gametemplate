/* globals AppGlobals, Crafty */
'use strict';

console.log("app.js - JavaScript Loaded");
console.log("CNS AppGlobals Available?: " + (AppGlobals !== undefined));

var AssetsManifest = {
	"images": ["/img/example.jpg", "/img/example.png"],
	"sprites": {
		"/img/glitch_walker.png": {
			paddingX: 0,
			paddingY: 0,
			paddingAroundBorder: 0,
			tile: 104,
			tileh: 114,
			map: {
				walker_start: [0, 0],
				walker_middle: [7, 0],
				walker_end: [7, 1]
			}
		}
	}
};

var Walker_SPT = null;
var Player_SPT = null;
var PLAYER_RIGHT = 0;
var PLAYER_LEFT = 1;


function PlayerFacing ( dir )
{
	if (dir === PLAYER_RIGHT && Walker_SPT.IsFacingRight_BOOL)  return;
	if (dir === PLAYER_LEFT && Walker_SPT.IsFacingRight_BOOL === false)  return;

	if (dir === PLAYER_RIGHT && Walker_SPT.IsFacingRight_BOOL === false) {
		Walker_SPT.unflip();
		Walker_SPT.IsFacingRight_BOOL = true;
		Player_SPT.unflip();
	}
	else if (dir === PLAYER_LEFT && Walker_SPT.IsFacingRight_BOOL) {
		Walker_SPT.flip("X");
		Walker_SPT.IsFacingRight_BOOL = false;
		Player_SPT.flip("X");
	}
}

function AddPlayer (  )
{
	Player_SPT = Crafty.e('2D, DOM, walker_start, SpriteAnimation, Twoway, Gravity, Gamepad')
		.attr({ x: 0, y: 0, w: 100, h: 100 })
		.twoway(200, 200)
		.gravity('Floor')
		.reel("walking", 1000, [
			[0, 0], [1, 0], [2, 0], [3, 0], [4, 0], [5, 0], [6, 0], [7, 0],
			[0, 1], [1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1], [7, 1]
		])
		.gamepad(0)
		.bind("KeyUp", function(e){
			switch (e.key) {
				case Crafty.keys.LEFT_ARROW :
					Walker_SPT.pauseAnimation().resetAnimation();
					Player_SPT.pauseAnimation().resetAnimation();
					break;
				case Crafty.keys.RIGHT_ARROW :
					Walker_SPT.pauseAnimation().resetAnimation();
					Player_SPT.pauseAnimation().resetAnimation();
					break;
				case Crafty.keys.UP_ARROW :
					break;
			}
		})
		.bind("KeyDown", function(e){
			switch (e.key) {
				case Crafty.keys.LEFT_ARROW :
					PlayerFacing(PLAYER_LEFT);
					Walker_SPT.animate("walking", -1);
					Player_SPT.animate("walking", -1);
					break;
				case Crafty.keys.RIGHT_ARROW :
					PlayerFacing(PLAYER_RIGHT);
					Walker_SPT.animate("walking", -1);
					Player_SPT.animate("walking", -1);
					break;
				case Crafty.keys.UP_ARROW :
					break;
			}
		})
		.bind('GamepadKeyChange', function (e) {
			switch (e.button) {
				case 14 :
					// DPad Left
					if (this.IsStickNavActive_BOOL === true)					return;
					switch (e.value) {
						case 0:
							Crafty.s('Keyboard').processEvent({'type':'keyup', 'keyCode' : Crafty.keys.LEFT_ARROW });
							break;
						case 1:
							Crafty.s('Keyboard').processEvent({'type':'keydown', 'keyCode' : Crafty.keys.LEFT_ARROW });
							break;
					}
					break;
				case 15 :
					// DPad Right
					if (this.IsStickNavActive_BOOL === true)					return;
					switch (e.value) {
						case 0:
							Crafty.s('Keyboard').processEvent({'type':'keyup', 'keyCode' : Crafty.keys.RIGHT_ARROW });
							break;
						case 1:
							Crafty.s('Keyboard').processEvent({'type':'keydown', 'keyCode' : Crafty.keys.RIGHT_ARROW });
							break;
					}
					break;
				case 0 :
					// Button A - Jump
					switch (e.value) {
						case 0:
							Crafty.s('Keyboard').processEvent({'type':'keyup', 'keyCode' : Crafty.keys.UP_ARROW });
							break;
						case 1:
							Crafty.s('Keyboard').processEvent({'type':'keydown', 'keyCode' : Crafty.keys.UP_ARROW });
							break;
					}
					break;
			}
		})
		.bind('GamepadAxisChange', function (e) {
			switch (e.axis) {
				// Left Stick
				case 0 :
					if (e.value > 0.5) {
						this.IsStickNavActive_BOOL = true;
						Crafty.s('Keyboard').processEvent({'type':'keydown', 'keyCode' : Crafty.keys.RIGHT_ARROW });
						break;
					}
					else if (e.value < -0.5) {
						this.IsStickNavActive_BOOL = true;
						Crafty.s('Keyboard').processEvent({'type':'keydown', 'keyCode' : Crafty.keys.LEFT_ARROW });
						break;
					}
					else if (this.IsStickNavActive_BOOL === true){
						this.IsStickNavActive_BOOL = false;
						Crafty.s('Keyboard').processEvent({'type':'keyup', 'keyCode' : Crafty.keys.RIGHT_ARROW });
						Crafty.s('Keyboard').processEvent({'type':'keyup', 'keyCode' : Crafty.keys.LEFT_ARROW });
					}
			}
		});
	Player_SPT.avoidCss3dTransforms = true;

	Walker_SPT = Crafty.e('2D, DOM, walker_start, SpriteAnimation')
		.reel("walking", 1000, [
			[0, 0], [1, 0], [2, 0], [3, 0], [4, 0], [5, 0], [6, 0], [7, 0],
			[0, 1], [1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1], [7, 1]
		]);
		//.animate("walking", -1);
	Walker_SPT.IsFacingRight_BOOL = true;
}


window.onload = function()
{
	Crafty.init(document.getElementById('game'));


	Crafty.e('Floor, 2D, Canvas, Color')
			.attr({x: 0, y: 250, w: Crafty.viewport._width, h: 10})
			.color('green');


	Crafty.load(AssetsManifest, AddPlayer);
};
