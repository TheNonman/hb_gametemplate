
/* globals KeyboardController, GamepadControllerAPI */

'use strict';


function StartGamepad (  )
{
	GamepadControllerAPI.registerGamepad(
		event.gamepad,
		[
			{
				key: GamepadControllerAPI.XBOX_GAMEPAD_MAPPING.D_PAD_UP,
				callback: function(){console.log("[HB_testing1  GamepadControllerAPI]  - D-Up");}
			},
			{
				key: GamepadControllerAPI.XBOX_GAMEPAD_MAPPING.D_PAD_DOWN,
				callback: function(){console.log("[HB_testing1  GamepadControllerAPI]  - D-Down");}
			}/*,
			{
			key: GamepadControllerAPI.XBOX_GAMEPAD_MAPPING.D_PAD_LEFT,
			callback: moveLeft
			},
			{
			key: GamepadControllerAPI.XBOX_GAMEPAD_MAPPING.D_PAD_RIGHT,
			callback: moveRight
			},
			{
			key: GamepadControllerAPI.XBOX_GAMEPAD_MAPPING.LEFT_BUMPER,
			callback: loadPrevPage
			},
			{
			key: GamepadControllerAPI.XBOX_GAMEPAD_MAPPING.RIGHT_BUMPER,
			callback: loadNextPage
			},
			{
			key: GamepadControllerAPI.XBOX_GAMEPAD_MAPPING.LEFT_TRIGGER,
			callback: changeTransactionMode
			},
			{
			key: GamepadControllerAPI.XBOX_GAMEPAD_MAPPING.RIGHT_TRIGGER,
			callback: changeTransactionMode
			},
			{
			key: GamepadControllerAPI.XBOX_GAMEPAD_MAPPING.Y,
			callback: sort
			},
			{
			key: GamepadControllerAPI.XBOX_GAMEPAD_MAPPING.A,
			callback: confirmButtonHandler
			},
			{
			key: GamepadControllerAPI.XBOX_GAMEPAD_MAPPING.B,
			callback: cancelButtonHandler
			}*/
		],
		200
	);
	GamepadControllerAPI.startListening();
}


window.addEventListener("load", function () {
	console.log("[HB_testing1]  ::  Window.load()");

	const keyboardNavigation = new KeyboardController(
		{
			event_type: "keydown",
			event_code: "ArrowDown",
			event_handler: function(){console.log("[HB_testing1  keyboardNavigation]  - ArrowDown");}
		},
		{
			event_type: "keydown",
			event_code: "ArrowUp",
			event_handler: function(){console.log("[HB_testing1  keyboardNavigation]  - ArrowUp");}
		}/*,
		{
		  event_type: "keydown",
		  event_code: "ArrowRight",
		  event_handler: moveRight
		},
		{
		  event_type: "keydown",
		  event_code: "ArrowLeft",
		  event_handler: moveLeft
		},
		{
		  event_type: "keydown",
		  event_code: "KeyQ",
		  event_handler: loadPrevPage
		},
		{
		  event_type: "keydown",
		  event_code: "KeyE",
		  event_handler: loadNextPage
		},
		{
		  event_type: "keydown",
		  event_code: "Enter",
		  event_handler: confirmButtonHandler
		},
		{
		  event_type: "keydown",
		  event_code: "Escape",
		  event_handler: cancelButtonHandler
		},
		{
		  event_type: "keydown",
		  event_code: "Digit1",
		  event_handler: sort
		},
		{
		  event_type: "keydown",
		  event_code: "KeyA",
		  event_handler: changeTransactionMode
		},
		{
		  event_type: "keydown",
		  event_code: "KeyD",
		  event_handler: changeTransactionMode
		}*/
	);

	console.log("keyboardNavigation: " + keyboardNavigation);

	window.addEventListener("gamepadconnected", function (event) {
		console.log("gamepadconnected !");
		//const TRIGGERS_MIN_VALUE = 0.5;
		StartGamepad();
	});

	// GamepadControllerAPI Will set listener for gamepaddisconnected


});
