# Hummingbird Game Template
### (HB_GameTemplate)

A quick setup template for getting started working on a game project targeting the Hummingbird Player.


# Prerequisites / Dependencies

1. [NodeJS](https://nodejs.org)

2. NodeJS NPM (*installed with NodeJS*)

6. The Grunt-CLI installed Globally  (After Node and NPM are installed, execute: `npm install -g grunt-cli`)

8.  *Optional*: The Karma-CLI installed Globally  (`npm install -g karma-cli`) The template itself currently makes no use of Karma, but is configured for it (with Jasmine tests).

# Getting Started

Assuming that you have the prerequisites installed on your environment...

1. Sync this Git repo

2. Open a Command Line / Terminal targeting the local repo directory.

    1. Execute: `npm install`
    4. Execute: `grunt Work`

Grunt will build a Staging site from the content in AppSource, launch that Staging site via an Express server, bind it up with BrowserSync and open the site in Chrome (assuming you have it installed) with dev file watches set for changes. 

When ready, in your Command Line / Terminal, hit Control-C to shut down the server and watches.

# Running in the Hummingbird Player

To load your project in the Hummingbird Player, you can launch the Hummingbird Player yourself and just drag and drop your html file from the _Staging folder on to the Hummingbird Player window.

For build flow integration, you can also provide the path to the Hummingbird Player on your machine by opening the default.env file and entering your path in HUMMINGBIRD_PLAYER_PATH

With this done, you can have the player launch your project's index.html by opening a Command Line / Terminal targeting the local repo directory and...

* To build and open in HB use: `grunt HB_Build`
	
	or
    
* If you build is up to date and you just want to launch use: `grunt HB`

If you are working with team members that don't have the Hummingbird Player installed in the same path location, the ‘default.env’ file in the root should be duplicated and renamed to “_local.env.” Open the _local.env and replace the value for HUMMINGBIRD_PLAYER_PATH with the proper path on your machine (examples and documentation included in the file). Note that files with this name are in the GitIgnore so all individuals can use them to configure per their local system setup without having to avoid checking in configs that are only relevant on their machine.

There are lots of options including, adding arguments to use when launching the player in HUMMINGBIRD_ARGS. The direct NodeJS child-process launch flow can instead use a middle-man shell script (either BAT on Windows or BASH on Mac/*nix).

ENV files will be used cascading as follows:

1. - If a '_local.env' file exists in the project directory, it will be used.

2. - Finally, as long as the 'default.env' file exists in the project directory, it will be used. 

# Build and Test

As noted above, the Work grunt task (executed with `grunt Work`) will build a fully navigable and testable Staging environment using a complete "source to temp to staging" build flow. It will then spin up a NodeJS Express web server bound to the produced staging environment ready to be accessed.

The Express web server is set by default to using port 3300. This can be changed by simply editing the Express server configuration in 'ServerConfigs/server_app.js'.

The Express web server is proxied by BrowserSync for its watch and update event features, exposing the proxy on port 3000. Thus, the main URL to load in a bowser when testing is `http://localhost:3000`. Of course, this also mean that if you change the main server port, you will want to also change the server URL that BrowserSync is set to proxy appropriately. This can be changed in the gruntfile. Just change in in the 'options' object of the 'browserSync' task definition.

# Grunt Task Help

Execute `grunt Help` (or just `grunt`) to get a list of available build and test tasks, with those tasks intended to be called directly by users specifically called out at the top of the list.

# Markdown and ReadMe References

If you want to learn more about editing this ReadMe and creating good markdown-based readme files, refer the following sources:

* [Adam Pritchard's Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

* [Markdown-Guide](http://markdown-guide.readthedocs.io/en/latest/basics.html)

* [Visual Studio ReadMe Guidelines](https://www.visualstudio.com/en-us/docs/git/create-a-readme)


