
const outputDebug_BOOL = true;
const envExt = ".env";

const fs = require('fs');
const envCmd = require('env-cmd');

function DebugLog( a_string )
{
	if (!outputDebug_BOOL)			return;
	console.log(a_string);
};


DebugLog("*** NODE JS CONTEXT: launch_hb.js ***\n");
DebugLog("Checking and Preparing the Configuration for Launching the Hummingbird Player");

let projectRoot = process.cwd();
DebugLog("Project CWD :  " + projectRoot + "\n");

let passArgs = [];
let envFile = null;

// Parse the Incoming CL Args
process.argv.forEach(function (val, index, array) {
	// Skip the first 2 (node location and current script path)
	if (index > 1) {
		let valueLC = val.toLowerCase();
		if (valueLC.endsWith(envExt)) {
			DebugLog("ENV file received in CL Args...");
			envFile = val;
			DebugLog("Path :  " + envFile + "\n");
		}
		else {
			passArgs.push(val);
		}
	}
});

if (!envFile) {
	DebugLog("\nNo ENV from CL - Looking for Standard ENV Files...");
	// No ENV in CL Args - Check Directories
	if (fs.existsSync("./_local.env")) {
		// Use Sub-Module ENV
		envFile = "./_local.env";
		DebugLog("-> Found Standard, Sub-Module Local ENV, Using: " + envFile);
	}
	else if (fs.existsSync("../_local.env")) {
		// Use Project Root Local ENV
		envFile = "../_local.env";
		DebugLog("-> Found Standard, Project Root Local ENV, Using: " + envFile);
	}
	else if (fs.existsSync("../default.env")) {
		// Use Project Root Default ENV
		envFile = "../default.env";
		DebugLog("-> Found Standard, Project Root DEFAULT ENV, Using: " + envFile);
	}
	else {
		console.log("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

		console.log("\nERROR");
		console.log("\nNo ENV File was received and none could be found!");
		console.log("Make sure that you have, at least, a 'default.env' file in the project root.");
		console.log("This should be checked in to the Project Repo.");
		console.log("There is no need to delete or change the 'default.env' file in the project root ");
		console.log("when its content is wrong for your environment. Copy it and rename it to ");
		console.log("'_local.env' instead. This will override use of the default.env.");

		console.log("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
		return;
	}
}
// Validate ENV File from CL Args for EnvCmd
else if (!fs.existsSync(envFile)) {
	console.log("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

	console.log("\nERROR");
	console.log("\nThe ENV file for use, does not exist !");
	console.log("Tried Path :  '" + envFile + "'");
	console.log("\nHummingbird Player launch cancelled !");

	console.log("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
	return;
}

// Add ENV File to use and follow-up script EnvCmd will call after loading from the ENV file
passArgs.unshift("./BuildScripts/start_hb_process.js");
passArgs.unshift("node");
passArgs.unshift(envFile);

DebugLog("Call EnvCmd");
DebugLog("Passing Args Array: " + passArgs.join(", ") + "\n");

envCmd.EnvCmd(passArgs);
