@echo off

echo CMD SCRIPT CONTEXT: starthb.bat

set localHB=%1
set projectRoot=%2
set hbStartPage=%3
set hbExtraArgs=%4
set batFileDIR=%~dp0

echo Local Hummingbird: %localHB%
echo Current Working Directory: %CD%
echo Project Directory: %projectRoot%
echo Extra HB Args: %hbExtraArgs%
echo BAT File DIR: %batFileDIR%
echo Start Page: %hbStartPage%

rem To change how the HBPlayer runs, change the arguments below.
rem Here's a list of useful arguments
rem Argument               | Meaning
rem --------------------------------------------
rem --url coui://uiresources/foo.html         | will load coui://uiresources/foo.html
rem --root /path/to/root   | sets the root directory for coui://
rem --vsync                | enables VSync; disabling VSync may freeze your computer due to HB running with thousands of FPS
rem --enable-complex-css   | enables support for complex CSS selectors (e.g. div > .class / a[href*="example"]
rem --debugger-port 9444   | sets the port to which the dev tools will listen; open http://localhost:<port> in Chrome to access them; set to -1 to disable
rem --log-verbosity debug  | tells the application how verbose the output should be; valid values are trace, debug, info, warning, assert and error. Defaults to info.
rem --input mouse          | tells the application what kind of input events to send; valid values are mouse and touch. Defaults to mouse.
start "HBPlayer" /d "%projectRoot%" "%localHB%" --player --url=coui://"%hbStartPage%" "--vsync"
