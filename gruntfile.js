'use strict';
//
//
// # NOTES
// ================================================================================================
// ================================================================================================



//
// # GRUNT / NODE	FILE	MATCHING	(GLOBBING)
// ----------------------------------------------
//
// Restrict matching to the content of one directory level down (i.e. single directory nesting):
//		'SomeRootDirectory/{,*/}*.js'
// Full Recursion, matching the content files in all subdirectoy levels:
//		'SomeRootDirectory/**/*.js'
// Full Recursion, matching All content:
//		'SomeRootDirectory/**'
// Skip a directory during recursion:
//		'!**/SkipThisDirectoryName/**'


//
//
// # Initialization of Any External Processor Objs
// ================================================================================================
// ================================================================================================

var LogFile = require("logfile-grunt");
var NodeSASS = require('node-sass');

//
//
// # Definition of Grunt Task Handling
// ================================================================================================
// ================================================================================================

module.exports = function(grunt)
{
	//
	// # Load Sub-modules (i.e. Grunt Tasks)
	// (Simplify with "matchdep" or "load-grunt-tasks" or get fancy with "load-grunt-config")
	// --------------------------------------------------------------------------------------------

	var Chalk = require("chalk");
	//var CommandExists = require('command-exists').sync;
	//var exec = require('child_process').exec;

	// Load All Grunt Task Types Listed in the Node package.json in devDependencies
	require('matchdep').filter('grunt-*').forEach(grunt.loadNpmTasks);

	grunt.task.loadTasks('./GruntTasks');


	//
	// # Helper Method Definitions
	// --------------------------------------------------------------------------------------------




	//
	// # Configure App Structure References
	// --------------------------------------------------------------------------------------------

	var BuildConfigs = {
		src: {
			root: "AppSource",
			css: "AppSource/SOURCE_STYLES",
			css_3P: "AppSource/SOURCE_STYLES/a_third_party",
			fonts: "AppSource/SOURCE_FONTS",
			html: "AppSource/SOURCE_HTML",
			images: "AppSource/SOURCE_IMAGES",
			js: "AppSource/SOURCE_JS",
			js_3P: "AppSource/SOURCE_JS/a_third_party"
		},
		temp: {
			root: "__Temp",
			css: "__Temp/css",
			css_3P: "__Temp/css/a_third_party",
			css_pre_source: "__Temp/css_sass_source",
			fonts: "__Temp/fonts",
			images: "__Temp/images",
			js: "__Temp/js",
			js_3P: "__Temp/js/a_third_party"
		},
		staging: {
			root: "_Staging",
			css: "_Staging/css",
			css_3P: "_Staging/css/a_third_party",
			fonts: "_Staging/fonts",
			images: "_Staging/img",
			js: "_Staging/js",
			js_3P: "_Staging/js/a_third_party"
		},
		logs: {
			base: "__BuildLogs"
		},
		staging_URLs: {
			main: "http://localhost:3000/"
		}
	};

	var JSHintOptions = {
		Production: {
			debug: false,
			node: true,
			browser: true,
			jquery: true, // Support for jQuery $ Def
			esnext: true,
			bitwise: true,
			curly: false,
			eqeqeq: false, // Suppress Warnings About Using == instead of ===
			immed: true,
			latedef: true,
			newcap: false,
			noarg: true,
			regexp: true,
			undef: true,
			unused: 'vars',  // Warn About Unused Variables but not Function Params (like e in an Event Handler)
			strict: true,
			trailing: false, // Allow Trailing Whitespace
			smarttabs: true,
			laxbreak: true,
			laxcomma: true, // Allow Comma-first Object Definition Syntax
			white: false // Don't Warn About Whitepace/tab Counts
		},
		Debug : {
			debug: true,
			node: true,
			browser: true,
			jquery: true, // Support for jQuery $ Def
			esnext: true,
			bitwise: true,
			curly: false,
			eqeqeq: false,  // Suppress Warnings About Using == instead of ===
			immed: true,
			latedef: true,
			newcap: false,
			noarg: true,
			regexp: true,
			undef: true,
			unused: 'vars',  // Warn About Unused Variables but not Function Params (like e in an Event Handler)
			strict: true,
			trailing: false, // Allow Trailing Whitespace
			smarttabs: true,
			laxbreak: true,
			laxcomma: true, // Allow Comma-first Object Definition Syntax
			white: false // Don't Warn About Whitepace/tab Counts
		}
	};


	var LogFileOptions = {
		filePath: BuildConfigs.logs.base + "/Grunt.log",
		clearLogFile: true,
		keepColors: false,
		textEncoding: "utf-8"
	};


	//
	// # Configure Grunt Tasks
	// --------------------------------------------------------------------------------------------

	grunt.initConfig({
		// Read in the Node package.json File So its Content is Available to Processes
		NodePkg: grunt.file.readJSON('package.json'),
		BuildConfigs: BuildConfigs,


		// Set tasks you define to be called by users in here to get them properly documented in Help requests.
		availabletasks: {
			tasks: {
				options: {
					groups: {
						'Main User Tasks': ['Work', 'MultiWork', 'HB', 'HB_Build', 'Docs', 'Nuke', 'Help']
					}
				}
			}
		},
		// End AvailableTasks Task Def


		browserSync: {
			options: {
				proxy: 'localhost:3300',// Main Server URL that BrowserSync will Proxy (i.e. the Express Server URL)
				port: 3000,// Port for the BrowserSync Local & External URLs - Default: 3000
				watchTask: true,// BrowserSync will be working with grunt-contrib-watch

				browser: ["chrome", "firefox", "safari"],// Browser(s) to trigger on startup of BrowserrSync
				// What URL to send to the configured browsers on startup of BrowserrSync
				open: "local", // Options: local, external, ui, ui-external, tunnel

				// Disable/enable mirrored events features
				ghostMode: {
					clicks: true,
					forms: true,
					scroll: true
				},

				// Configuration of the BrowserSync web UI
				ui: {
					port: 3001,// Port for the BrowserSync Web UI on both Local & External URLs - Default: 3001
					// If a Weinre Server is running, the Port it is using
					/*
					weinre: {
						port: 9090
					}
					*/
				}
			},
			dev_mac: {
				bsFiles: {
					// BrowserSync should watch and trigger browser events based on changes to the
					// built files in staging
					src: '<%= BuildConfigs.staging.root %>/**/*'
				},
				options: {
					browser: ["google chrome"],
				}
			},
			dev_mac_all: {
				bsFiles: {
					src: '<%= BuildConfigs.staging.root %>/**/*'
				},
				options: {
					browser: ["google chrome", "firefox", "safari"],
				}
			},
			dev_nix: {
				bsFiles: {
					src: '<%= BuildConfigs.staging.root %>/**/*'
				},
				options: {
					browser: ["chrome"],
				}
			},
			dev_nix_all: {
				bsFiles: {
					src: '<%= BuildConfigs.staging.root %>/**/*'
				},
				options: {
					browser: ["chrome", "firefox"],
				}
			},
			dev_win: {
				bsFiles: {
					src: '<%= BuildConfigs.staging.root %>/**/*'
				},
				options: {
					browser: ["chrome"],
				}
			},
			dev_win_all: {
				bsFiles: {
					src: '<%= BuildConfigs.staging.root %>/**/*'
				},
				options: {
					browser: ["chrome", "firefox"],
				}
			}
		},
		// End BrowserSync Task Def


		browserSync_ByPlatform : {
			one: {},
			all: {}
		},


		clean: {
			full: {
				options: { 'no-write': false },
				src: [	'<%= BuildConfigs.temp.root %>',
						'<%= BuildConfigs.staging.root %>'
				]
			},
			full_test: {
				options: { 'no-write': true },// Don't Actually Delete Anything
				src: [	'<%= BuildConfigs.temp.root %>',
						'<%= BuildConfigs.staging.root %>'
				]
			},
			strip_to_source: {
				options: { 'no-write': false },
				src: [	'<%= BuildConfigs.temp.root %>',
						'<%= BuildConfigs.staging.root %>',
						'.sass-cache',
						'__BuildLogs',
						'node_modules'
				]
			}
		},
		// End Clean Task Defs


		copy: {
			image_files_ToTemp: {
				files: [
					{
						expand: true,
						cwd: '<%= BuildConfigs.src.images %>',
						src: ['**/*.jpg', '**/*.png', '**/*.gif', '**/*.svg'],
						dest: '<%= BuildConfigs.temp.images %>'
					}
				]
			},
			image_files_ToStaging: {
				files: [
					{
						expand: true,
						cwd: '<%= BuildConfigs.temp.images %>',
						src: ['**/*.jpg', '**/*.png', '**/*.gif', '**/*.svg'],
						dest: '<%= BuildConfigs.staging.images %>'
					}
				]
			},
			// Copy all Manually Written Preprocessor CSS Source to temp
			pre_css_source: {
				files: [
					{
						expand: true,
						cwd: '<%= BuildConfigs.src.css %>',
						src: ['**/*.scss', '**/*.sass'],
						dest: '<%= BuildConfigs.temp.css_pre_source %>'
					}
				]
			}
		},
		// End Copy Task Defs


		express: {
			options: {
				// Object with properties `out` and `err` both will take a path to a log file and
				// append the output of the server. Make sure the folders exist.
				logs: {
					out: '__BuildLogs/serverOutput.log',
					err: '__BuildLogs/serverErrors.log'
				}
			},
			dev: {
				options: {
					script: 'ServerConfigs/server_app.js'
				}
			},
			prod: {
				options: {
					script: 'ServerConfigs/server_app.js',
					node_env: 'production'
				}
			},
			test: {
				options: {
					script: 'ServerConfigs/server_app.js'
				}
			}
		},
		// End Express Task Defs


		jsdoc : {
			dist : {
				src: ['<%= BuildConfigs.src.js %>/**/*.js', 'README.md', '!**/a_third_party/**'],
				options: {
					destination: 'Documentation/API_Docs'
				}
			}
		},
		// End JSDoc Task Defs


		jshint: {
			sourceDebug: {// Tests only this Gruntfile and custom scripts the AppSource
				// JSHint Options
				src: ['gruntfile.js',
					'<%= BuildConfigs.src.js %>/**/*.js',
					'!<%= BuildConfigs.src.js_3P %>/**/*.js'
				],
				options: JSHintOptions.Debug
			},
			sourceProduction: {// Tests only this Gruntfile and custom scripts the AppSource
				// JSHint Options
				src: ['gruntfile.js',
					'<%= BuildConfigs.src.js %>/**/*.js'
				],
				options: JSHintOptions.Production
			},
			grunt: {// Tests only this Gruntfile with the included JSHint Options
				src: ['gruntfile.js'],
				options: JSHintOptions.Production
			}
		},
		// End JSHint Task Defs


		run: {
			launch_hb: {
				cmd: 'node',
				args: [
					'BuildScripts/launch_hb.js',
					'_Staging/index.html'
				]
			}
		},
		// End JSHint Task Defs


		sass: {
			options: {
				implementation: NodeSASS,
				outputStyle: 'nested'
				//sourceComments: true,
				//sourceMap: true
			},
			dist: {
				expand: true,
				cwd: '<%= BuildConfigs.temp.css_pre_source %>',
				src: ['**/*.scss'],
				dest: '<%= BuildConfigs.temp.css %>',
				ext: '.css'
			}
		},
		// End SASS Task Defs


		sync: {
			css_ToStaging: {
				files: [
					{ cwd: '<%= BuildConfigs.temp.css %>', src: ['**/*.css'], dest: '<%= BuildConfigs.staging.css %>' },
					// Syncs All Non_CSS Content in 'a_third_party' to Include Resource Fonts, Images, SWFs, etc. of 3rd Party CSS Libraries
					{ cwd: '<%= BuildConfigs.temp.css_3P %>', src: ['**/*', '!**/*.css'], dest: '<%= BuildConfigs.staging.css_3P %>' }
				],
				pretend: false, // Simulation only mode - no actual file changes.
				verbose: true, // Display log messages when copying (or simulating copying) files
				updateAndDelete: true // Remove all files from destination that are not in src. Default:  false
				//ignoreInDest: 'none', // Never remove from destination. Default: none
				//compareUsing: 'md5' // Comparison Mode: 'md5' file hashes or file modification time, 'mtime'
			},
			css_ToTemp: {
				files: [
					{ cwd: '<%= BuildConfigs.src.css %>', src: ['**/*.css'], dest: '<%= BuildConfigs.temp.css %>' },
					// Syncs All Non_CSS Content in 'a_third_party' to Include Resource Fonts, Images, SWFs, etc. of 3rd Party CSS Libraries
					{ cwd: '<%= BuildConfigs.src.css_3P %>', src: ['**/*', '!**/*.css', '!**/*.scss'], dest: '<%= BuildConfigs.temp.css_3P %>' }
				],
				pretend: false, // Simulation only mode - no actual file changes.
				verbose: true, // Display log messages when copying (or simulating copying) files
				updateAndDelete: false // Remove all files from destination that are not in src. Default:  false
				//ignoreInDest: 'none', // Never remove from destination. Default: none
				//compareUsing: 'md5' // Comparison Mode: 'md5' file hashes or file modification time, 'mtime'
			},
			fonts_ToStaging: {
				files: [
					{ 	cwd: '<%= BuildConfigs.src.fonts %>',
						src: ['**/*.eot', '**/*.svg', '**/*.ttf', '**/*.woff'],
						dest: '<%= BuildConfigs.staging.fonts %>' }
				],
				pretend: false, // Simulation only mode - no actual file changes.
				verbose: true, // Display log messages when copying (or simulating copying) files
				updateAndDelete: false // Remove all files from destination that are not in src. Default:  false
				//ignoreInDest: 'none', // Never remove from destination. Default: none
				//compareUsing: 'md5' // Comparison Mode: 'md5' file hashes or file modification time, 'mtime'
			},
			html_ToStaging: {
				files: [
					{ cwd: '<%= BuildConfigs.src.html %>',
					src: ['**/*.html'],
					dest: '<%= BuildConfigs.staging.root %>' },
				],
				pretend: false, // Simulation only mode - no actual file changes.
				verbose: true, // Display log messages when copying (or simulating copying) files
				updateAndDelete: false // Remove all files from destination that are not in src. Default:  false
				//ignoreInDest: 'none', // Never remove from destination. Default: none
				//compareUsing: 'md5' // Comparison Mode: 'md5' file hashes or file modification time, 'mtime'
			},
			js_ToStaging: {
				files: [
					{ cwd: '<%= BuildConfigs.temp.js %>', src: ['**/*.js'], dest: '<%= BuildConfigs.staging.js %>' },
					// Syncs All Non_JavaScript Content to Include Resource CSS, Images, SWFs, etc. of JavaScript Libraries
					{ cwd: '<%= BuildConfigs.temp.js %>', src: ['**/*' , '!**/*.js'], dest: '<%= BuildConfigs.staging.js %>' }
				],
				pretend: false, // Simulation only mode - no actual file changes.
				verbose: true, // Display log messages when copying (or simulating copying) files
				updateAndDelete: true // Remove all files from destination that are not in src. Default:  false
				//ignoreInDest: 'none', // Never remove from destination. Default: none
				//compareUsing: 'md5' // Comparison Mode: 'md5' file hashes or file modification time, 'mtime'
			},
			js_ToTemp: {
				files: [
					// Syncs All Content (except CoffeeScripts) to Include Resource CSS, Images, SWFs, etc. of JavaScript Libraries
					{ cwd: '<%= BuildConfigs.src.js %>', src: ['**/*', '!**/__NOT_IN_USE/**', '!**/amd_modules/**',	'!**/*.coffee'], dest: '<%= BuildConfigs.temp.js %>' }
				],
				pretend: false, // Simulation only mode - no actual file changes.
				verbose: true, // Display log messages when copying (or simulating copying) files
				updateAndDelete: false // Remove all files from destination that are not in src. Default:  false
				//ignoreInDest: 'none', // Never remove from destination. Default: none
				//compareUsing: 'md5' // Comparison Mode: 'md5' file hashes or file modification time, 'mtime'
			},
			js3P_ToTemp: {
				files: [
					// Syncs All Content (except CoffeeScripts) to Include Resource CSS, Images, SWFs, etc. of JavaScript Libraries
					{ cwd: '<%= BuildConfigs.src.js_3P %>', src: ['**/*', '!**/__NOT_IN_USE/**', '!**/amd_modules/**',	'!**/*.coffee'], dest: '<%= BuildConfigs.temp.js_3P %>' }
				],
				pretend: false, // Simulation only mode - no actual file changes.
				verbose: true, // Display log messages when copying (or simulating copying) files
				updateAndDelete: false // Remove all files from destination that are not in src. Default:  false
				//ignoreInDest: 'none', // Never remove from destination. Default: none
				//compareUsing: 'md5' // Comparison Mode: 'md5' file hashes or file modification time, 'mtime'
			}
		},
		// End Sync Task Defs


		watch: {
			options: {
				// LiveReload has been disabled in the watch flow in favor of using BrowserSync
				// To use livereload uncomment this port declaration and remove use of the browserSync task
				//livereload: 35366 // Port for the livereload server - Default is 35729
					/// SSL Support
					// key: grunt.file.read('path/to/ssl.key'),
					// cert: grunt.file.read('path/to/ssl.crt')
					/// You can pass in any other options you'd like to the https server,
					/// as listed here:
					/// http://nodejs.org/api/tls.html#tls_tls_createserver_options_secureconnectionlistener
			},
			sass: {
				files: ['<%= BuildConfigs.src.css %>/**/*.scss'],
				tasks: ['copy:pre_css_source', 'sass', 'sync:css_ToStaging']
			},
			css: {
				files: ['<%= BuildConfigs.src.css %>/**/*.css'],
				tasks: ['sync:css_ToTemp', 'sync:css_ToStaging']
			},
			html: {
				files: ['<%= BuildConfigs.src.html %>/**/*.html'],
				tasks: ['sync:html_ToStaging']
			},
			js: {
				files: ['<%= BuildConfigs.src.js %>/**/*.js', '!<%= BuildConfigs.src.js_3P %>/**/*.js'],
				tasks: ['jshint:sourceDebug', 'sync:js_ToTemp', 'sync:js_ToStaging']
			}
		}
		// End Watch Task Defs

	});


	//
	// # Register (Executable) Grunt Task Aliases
	// --------------------------------------------------------------------------------------------

	// Define the DEFAULT task - executed when running grunt with no arguments
	grunt.registerTask('default', ['Help']);

	grunt.registerTask('BasicBuild', 'Basic One-time build (No Watches Set).',
		function() {
			grunt.task.run([
				'StartProcessLog',
				'jshint:sourceDebug',
				'clean:full',
				'copy:pre_css_source',
				'sass',
				'sync:js_ToTemp',
				'sync:js3P_ToTemp',
				'sync:css_ToTemp',
				'sync:css_ToStaging',
				'sync:js_ToStaging',
				'sync:html_ToStaging',
				'copy:image_files_ToTemp',
				'copy:image_files_ToStaging',
				'sync:fonts_ToStaging',
			]);
		}
	);


	grunt.registerTask('Docs', 'Generate (Update) the API documentation with JSDoc.',
		function() {
			grunt.task.run([
				'StartProcessLog',
				'jsdoc'
			]);
		}
	);


	grunt.registerTask('Help', 'Print project info and available grunt tasks.  < DEFAULT >',
		function() {
			grunt.task.run([
				'StartProcessLog',
				'availabletasks'
			]);
		}
	);


	grunt.registerTask('Nuke', 'Clears All Build Content and Strips Down to Only Source (NPM Install will be Required to Work Again).',
		function() {
			grunt.task.run([
				'clean:strip_to_source'
			]);
		}
	);


	grunt.registerTask('MultiWork', 'Starts the Development Build Cycle, Express Web Server, and Watches. Opens and updates in Chrome AND All Other Registered Browsers.',
		function() {
			grunt.task.run([
				'BasicBuild',
				'express:dev',
				'browserSync_ByPlatform:all',
				'watch'
			]);
		}
	);


	grunt.registerTask('Work', 'Starts the Development Build Cycle, Express Web Server, and Watches. Opens and updates in Chrome only.',
		function() {
			grunt.task.run([
				'BasicBuild',
				'express:dev',
				'browserSync_ByPlatform:one',
				'watch'
			]);
		}
	);


	grunt.registerTask('HB', 'Starts Hummingbird Player (without building).',
		function() {
			grunt.task.run([
				'run:launch_hb'
			]);
		}
	);


	grunt.registerTask('HB_Build', 'Runs the Build Cycle and Starts Hummingbird Player.',
		function() {
			grunt.task.run([
				'BasicBuild',
				'run:launch_hb'
			]);
		}
	);


	// Default task(s).
	grunt.registerTask('StartProcessLog', 'Adds Process Info in the Log and Console',
		function() {

			new LogFile(grunt, LogFileOptions);

			grunt.log.writeln();
			grunt.log.writeln();
			grunt.log.writeln();
			grunt.log.writeln(Chalk.green('Starting Grunt Tasking for...'));
			grunt.log.writeln(Chalk.green('	->	NPM Project Name: ' + grunt.config('NodePkg.name')));
			grunt.log.writeln(Chalk.green('	->	NPM Project Version: ' + grunt.config('NodePkg.version')));
			grunt.log.writeln(Chalk.green('	->	Using Grunt Version: ' + Chalk.blue(grunt.version)));

			var startTime = new Date();
			grunt.log.writeln(Chalk.green('	->	Process Started: ' + Chalk.blue(startTime)));
			grunt.log.writeln(Chalk.green('================================================================================'));
			grunt.log.writeln(Chalk.green('================================================================================'));
			grunt.log.writeln("");
		}
	);
};
